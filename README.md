# AJAX - Taller code-review Sigma

- Realizar un fork de este repositorio
- Completar la funcionalidad AJAX
    - URL: `https://code.nunzioproject.xyz/ajax.php`
    - Method: `GET`
- Renderizar la información del **response** y mostrarla en el HTML con sus etiquetas correspondientes, se evalua la semantica del codigo.
- Utilizar el `gitlab-ci.yml` para crear una **gitlab page** del proyecto.


Ejemplo del response:

```
{
    "band-name": "Dummy Band",
    "album-name": "Dummy Album",
    "genres": ["French house","Nu-disco"],
    "image": "/img/dummy.jpg",
    "location": {
        "city": "Medellin",
        "country": "Colombia"
    }
}
```